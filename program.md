Topics
======

1. Python (all of Monday)
    - Not sure which material to use. Maybe http://www.python-course.eu/
    - Give assignments such as "find all arXiv papers citing a particular source" and assignments from eulerproject.org
    - Start from a crash course, end the day with more complex assignments
2. Unix shell (Tuesday morning)
    - A brief intro on what the shell is about, directory structure,
list of shell commands
3. Version control/Git (Tuesday afternoon)
    - Find a tutorial; perhaps software carpentry
4. Programme structure (Wednesday)
    - Assignment: project which can be split into 2 parts, to be done by
2 guys. Then: join the two bits
      Perhaps (if time permits) do this once without explaining the
importance of structure, then give the info and give a 2nd
      assignment.
5. Data storage + processing + hardware (Thursday)
    - use scikit-image. Denoisify a picture, find contor lines, min/max
threshold, 3D data sets (?)
    - file formats: netcdf, hdf5
6. Different computer language types (Friday)
    - use C(++) or F90 within python
7. Copyright, sharing, licencing (Friday)
8. HPC (Friday)
    - Parallelism; Shared and distributed memory systems

The idea is that students prepare themselves for each day using online
material. That is then summarized and then the
students start practicing. This allows us to help them individually,
giving more support to the people with deficient backgrounds

Exercises should be split into an essential/basic part and bells and
whistles which can be done by the faster students.
