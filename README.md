Teaching and organizational materials for the Casimir programming course.

Copyright 2015, TU Delft.

**Contents**:

[Organizational details](planning.md)

[Instructional plan](program.md)
