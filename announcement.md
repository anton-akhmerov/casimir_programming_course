The majority of scientists encounter tasks where programming is either helpful or even required.
With relatively little training, you can solve these tasks in a fast, systematic, and clean way while also making your solutions reusable for yourself and others.
This one-week long intensive workshop will give you this training.
You will learn:

 * Basics of programming: the ABCs.
 * The unix shell: a different view on your files.
 * Using libraries: stand on the shoulders of giants.
 * Structuring your program: no spaghetti code.
 * Testing: are you really sure there are no bugs?
 * Version control: this worked yesterday!
 * Data storage and processing: how to not get lost.
 * Programming languages: from C to Matlab.
 * Next steps: publishing your code, using supercomputers, and more.
