Unordered organizational information about the course
=====================================================

* Course dates: Nov 2-6
* Schedule: 2 sessions 3-4 hours each with coffee breaks and lunch served between the sessions, 1 hour reading to prepare for the next day. Drinks at the end of the course.
* Participants: priority to PhDs. Total of 50 places, working in pairs.
* At least 5 instructors available at every moment, 5 groups per instructor.
* What kind of computational environment shall we use? Jupyterhub for everyone?
* How will we host git repos?

Planning discussion Sept 17
===========================

* We need to update the jupyterhub setup on hpc with OAuth
* Maybe even docker to not pollute the users
* Use local repos and explain the decentralized fetch workflow before showing github
* Perhaps attach a temperature sensor or some other simple hardware to HPC2
* Project Euler for starting tasks
* Python intro:
  + Can probably borrow from [Michael's course](https://gitlab.com/michaelwimmer/trieste-kwant-course)
  + Simple crypto for the same thing?
  + Some web stuff with an API? (arxiv, twitter, email)
  + Maybe more require
* Shell/Git:
  + Probably use Software Carpentry
* Program advanced:
  + Tests
  + modules
